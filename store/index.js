import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

Vue.use(Vuex)
Vue.use(VueRouter)

const store = () => new Vuex.Store({
  state: {
    home: '/home',
    openModal: {
      modalname: [],
      option: {}
    },
    notes: []
  },
  mutations: {
    OPEN_MODAL: function (state, { modalname, option }) {
      state.openModal = { modalname, option }
    },
    SET_NOTES: function (state, notes) {
      state.notes = notes
    },
  },
  actions: {
    async openModal({ commit }, { modalname, option }) {
      commit('OPEN_MODAL', { modalname, option })
    },
    async set_notes({ commit }) {
      let result = await this.$axios.get('/notes')
      if(result && result.data && result.data.notes && result.data.notes.length >= 0) {
        commit('SET_NOTES', result.data.notes)
      }
    },
    async update_notes({ commit }) {
      try {
        let result = await this.$axios.patch('/notes')
        if(result && result.data && result.data.notes && result.data.notes.length >= 0) {
          commit('SET_NOTES', result.data.notes)
        }
      } catch(e) {
        let login_url = await this.$axios.get('/google');
        if(login_url && login_url.data && login_url.data.url) {
          window.open(login_url.data.url, '_self');
        }
      }
    }
  }
})

export default store
