export default {
  env: {
    REQ_SER_HOST: process.env.REQ_SER_HOST
  },
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: '個人網站',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      // { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: 'https://polyfill.io/v3/polyfill.min.js?flags=gated&features=default' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/magnific-popup.css',
    // { lang: 'scss', src: '@/assets/sass/color.scss' }
    { src: '~/node_modules/highlight.js/styles/tomorrow-night-eighties.css', lang: 'css' }
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vuex.js',
    '~/plugins/main.js',
    '~/plugins/ws',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/markdownit'
  ],
  // markdown
  markdownit: {
    injected: true, //$md
    html: true, // Enable HTML tags in source
    linkify: true, //Autoconvert URL-like text to links
    breaks: true,        // Convert '\n' in paragraphs into <br>
    use: [
      'markdown-it-highlightjs'
    ]
  },
  // bootstrap
  bootstrapVue: {
    componentPlugins: [
      'LayoutPlugin',
      'FormPlugin',
      'FormCheckboxPlugin',
      'FormInputPlugin',
      'FormRadioPlugin',
      'ToastPlugin',
      'ModalPlugin'
    ],
    directivePlugins: ['VBPopoverPlugin', 'VBTooltipPlugin', 'VBScrollspyPlugin']
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: `${ process.env.REQ_SER_HOST || 'http://localhost:4000' }/api`
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
