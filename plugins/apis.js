import Vue from 'vue';
import google from '~/apis/google.js';

let apis = {
  computed: {
    apis() {
      google.init(this);

      return {
        google,
      }
    }
  },
  methods: {
    // 處理axios狀況
    async send_packet({ method, url, data = {}, option = {} }, callback = () => {}) {
      let result;

      try {
        if(![ 'get', 'post', 'put', 'patch', 'delete' ].includes(method) || !url || url == '') {
          throw {
            message: 'METHOD or URL is error',
            method,
            url
          }
        }
        switch(method) {
          case 'get':
            if(Object.keys(data).length > 0) {
              Object.entries(data).forEach(([key, value]) => {
                if(/\?/.test(url)) {
                  url += `&${ key }=${ value }`;
                } else {
                  url += `?${ key }=${ value }`;
                }
              })
            }
            result = await this.$axios.$get(url, option);
            break;
          case 'post':
            result = await this.$axios.$post(url, data, option);
            break;
          case 'put':
            result = await this.$axios.$put(url, data, option);
            break;
          case 'patch':
            result = await this.$axios.$patch(url, data, option);
            break;
          case 'delete':
            result = await this.$axios.$delete(url);
            break;
        }
        await callback(result);
      } catch(err) {
        if(err.response) {
          console.error(err.response)
        } else {
          console.error(err)
        }
      }

      return result;
    }
  }
}
export default apis