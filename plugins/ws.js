import Vue from 'vue'
import Ws from '@adonisjs/websocket-client'

const wsVuePlugin = function (Vue, url, options) {
  Vue.prototype.$io = Ws(url, options);
  Vue.prototype.$io.connect();
  Vue.prototype.$io.on('open', () => {
    console.log('WebSocket is open');
  });
  Vue.prototype.$io.on('close', () => {
    console.log('WebSocket is close');
  });
}

let REQ_SER_HOST = process.env.REQ_SER_HOST || 'http://0.0.0.0:4000';
// http:// -> ws://     https:// -> wss://
Vue.use(wsVuePlugin, REQ_SER_HOST.replace(/http(s?):\/\//, 'ws$1://'), {});